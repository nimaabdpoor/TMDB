package com.nima.tmdb.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nima.tmdb.R

class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }
}