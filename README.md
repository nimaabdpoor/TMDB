# THE MOVIE DATABASE API

  ![first page](https://github.com/nima-abdpoor/TMDB/blob/kotlin/app/src/main/res/raw/one.jpg)![second page](https://github.com/nima-abdpoor/TMDB/blob/kotlin/app/src/main/res/raw/two.jpg)
  ![third page](https://github.com/nima-abdpoor/TMDB/blob/kotlin/app/src/main/res/raw/three.jpg)

# Features
 - Search Movie name 
 - Show Details Of Selected Movie
 
# Technologies
 - kotlin
 - MVVM architecture
 - Navigaion Components with animation
 - DiffUtils-RecyclerView
 - Retrofit2
 - Coroutines
 
# Refrence
 - [coding with mitch course](https://codingwithmitch.com/courses/rest-api-mvvm-retrofit2/)
 

# Future
 - getting and saving recommendations of selected movie(dettails of movies) to show theme when onResume Called.
